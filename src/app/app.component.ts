import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

enum abecedary {
  A,
  B,
  C,
  D,
  E,
  F,
  G,
  H,
  I,
  J,
  K,
  L,
  M,
  N,
  Ñ,
  O,
  P,
  Q,
  R,
  S,
  T,
  U,
  V,
  W,
  X,
  Y,
  Z,
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  enigmaForm: FormGroup;
  enigmaResult: string;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.enigmaForm = this.fb.group({
      enigma: ['KJPNE INF IJ PTX UWTLWFQFITWJX', Validators.required],
    });
  }

  solveTheEnigma() {
    if (this.enigmaForm.invalid) {
      return;
    }
    const { enigma } = this.enigmaForm.value;
    this.enigmaResult = this.calculateEnigma(enigma);
  }

  private calculateEnigma(enigma: string) {
    let result = '';
    for (const i of enigma) {
      let character: string;
      const j: any = i.trim();
      if (!j) {
        console.log(' ');
        character = ' ';
      } else {
        const numberEnigmaLetter: any = abecedary[j] as any;
        let numberResultLetter = numberEnigmaLetter - 5;
        if (numberResultLetter < 0) {
          numberResultLetter = 27 + numberResultLetter;
        }
        console.log(abecedary[numberResultLetter]);
        character = abecedary[numberResultLetter];
      }
      result += character;
    }
    return result;
  }
}
